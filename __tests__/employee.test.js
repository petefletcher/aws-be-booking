const request = require('supertest');
const server = 'http://localhost:3000';
const AWS = require('aws-sdk');


//create db instance to write to
const dynamoDb = new AWS.DynamoDB({
    region: 'localhost',
    endpoint: 'http://localhost:8000',
});

//perform once berfore all tests
beforeAll(async () => {
    var params = {
        TableName: 'employeesTable',
        KeySchema: [ // The type of of schema.  Must start with a HASH type, with an optional second RANGE.
            { // Required HASH type attribute
                AttributeName: 'email',
                KeyType: 'HASH',
            }
        ],
        AttributeDefinitions: [ // The names and types of all primary and index key attributes only
            {
                AttributeName: 'email',
                AttributeType: 'S', // (S | N | B) for string, number, binary
            }
        ],
        ProvisionedThroughput: { // required provisioned throughput for the table
            ReadCapacityUnits: 1, 
            WriteCapacityUnits: 1
        }
    };

    await dynamoDb.createTable(params, function(err, data) {
        if (err) console.log(err); // an error occurred
        else console.log("User table created"); // successful response
    });

    var empParams = {
        TableName: 'usersTable',
        KeySchema: [ // The type of of schema.  Must start with a HASH type, with an optional second RANGE.
            { // Required HASH type attribute
                AttributeName: 'email',
                KeyType: 'HASH',
            }
        ],
        AttributeDefinitions: [ // The names and types of all primary and index key attributes only
            {
                AttributeName: 'email',
                AttributeType: 'S', // (S | N | B) for string, number, binary
            }
        ],
        ProvisionedThroughput: { // required provisioned throughput for the table
            ReadCapacityUnits: 1, 
            WriteCapacityUnits: 1
        }
    };
    await dynamoDb.createTable(empParams, function(err, data) {
        if (err) console.log(err); // an error occurred
        else console.log("Employee table created"); // successful response
    });
})

afterAll(async () => {
    var params = {
        TableName: 'usersTable',
    };

    await dynamoDb.deleteTable(params, function(err, data) {
        if (err) console.log(err); // an error occurred
        else {
        return; 
        }
    });
    
    var empParams = {
        TableName: 'employeesTable',
    };

    await dynamoDb.deleteTable(empParams, function(err, data) {
        if (err) console.log(err); // an error occurred
        else {
        return; 
        }
    });
});

const testUser = {
    "email": "testuser@email.com",
    "password": "password",
    "confirmPassword": "confirmPassword",
    "firstName": "firstName",
    "lastName": "lastName",
    "mobile": "mobile",
    "imageURL": "imageURL"

};

const testUserEdit = {
    "email": "testuser@email.com",
    "services": ["tattoo", "piercing"]
}

const testInvalidPut = {
    "email": "testuser@email.com",
    "password": "passwordedit1",
    "confirmPassword": "confirmPasswordedit1",
    "firstName": "firstNameedit1",
    "lastName": 1,
    "mobile": "mobileedit1",
    "imageURL": "imageURL1"
};

const testUserInvalidPOST = {
    "email": "testuser@email.com",
    "services": "tattoo"
};

describe('POST api/createEmployee/{email}', () => {
    test("creates an employee", async () => {
        const newUser = await request(server)
            .post('/api/user')
            .send(testUser);

        const newEmployee = await request(server)
            .post(`/api/createEmployee/${testUser.email}`)
            .send({
                "services": ["tattoo", "laser"]
            });
        
        //make sure we added the user correctly
        expect(newEmployee.body).toHaveProperty("services");
        expect(newEmployee.body.services).toEqual([ 'tattoo', 'laser' ]);
        expect(newEmployee.statusCode).toBe(201);
    });
})

describe('GET /getAllEmployees', () => {
    test("Gets all employees from employee table", async () => {
       
        const response = await request(server).get('/api/getAllEmployees');
        expect(response.body.length).toBe(1)
        expect(response.body[0].email).toBeTruthy();
        expect(response.body[0].email).toEqual(testUser.email);
        expect(response.statusCode).toBe(200); 
    });
})

describe('PUT /api/editEmployee/{email}', () => {
    test("Updates a employee from employee table", async () => {
        const response = await request(server)
            .put(`/api/editEmployee/${testUser.email}`)
            .send(testUserEdit);

        expect(response.body.email).toBeTruthy();
        expect(response.body.services).not.toEqual(testUser.services);
        expect(response.statusCode).toBe(200); 
    });
})

//Error tests
describe('POST api/createEmployee/{email}', () => {
    test("Error when employee payload not valid ", async () => {
        const newUser = await request(server)
            .post('/api/user')
            .send(testUser);

        const response = await request(server).post(`/api/createEmployee/${testUser.email}`).send(testUserInvalidPOST);    
        expect(response.body.error).toBeTruthy();
        expect(response.body.error).toEqual('"services" must be an array');;
        expect(response.statusCode).toBe(404); 
    });
})

//Delete
describe('DELETE api/deleteEmployee/{email}', () => {
    test("Deletes a employee from employee table", async () => {
        const response = await request(server)
            .delete(`/api/deleteEmployee/${testUser.email}`)

        expect(response.body.message).toEqual('Employee deleted successfully');
        expect(response.statusCode).toBe(200); 
        
        const responseUser = await request(server).get(`/api/getEmployee/${testUser.email}`);
        expect(responseUser.body.error).toEqual('Employee not found');

        const responseGetAll = await request(server).get('/api/getAllEmployees');
        expect(responseGetAll.statusCode).toBe(404);
        expect(responseGetAll.error.text).toEqual(JSON.stringify('No employees in table'));
    });
})
describe('DELETE api/deleteEmployee/{email}', () => {
    test("Error is no user to delete", async () => {
        const response = await request(server)
            .delete(`/api/deleteEmployee/testEmail@delete.com`)
            expect(response.statusCode).toBe(404);
            expect(response.body.error).toEqual('Employee not found');
    });
})

describe('GET /getAllEmployees', () => {
    test("Returns correct response when no employees in the table", async () => {
       
        const response = await request(server).get('/api/getAllEmployees');
        expect(response.statusCode).toBe(404);
        expect(response.error.text).toEqual(JSON.stringify('No employees in table')); 
    });
})

describe('GET /getEmployee', () => {
    test("Error when Employee does not exist", async () => {
        const response = await request(server).get(`/api/getEmployee/${testUser.email}`);
        expect(response.body.error).toBeTruthy();
        expect(response.body.error).toEqual('Employee not found');;
        expect(response.statusCode).toBe(404); 
    });
})