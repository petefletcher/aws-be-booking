const userFunctions = require('../lambda/users');
const request = require('supertest');
const server = 'http://localhost:3000';
const AWS = require('aws-sdk');


//create db instance to write to
const dynamoDb = new AWS.DynamoDB({
    region: 'localhost',
    endpoint: 'http://localhost:8000',
});

//perform once berfore all tests
beforeAll(async () => {
    var params = {
        TableName: 'usersTable',
        KeySchema: [ // The type of of schema.  Must start with a HASH type, with an optional second RANGE.
            { // Required HASH type attribute
                AttributeName: 'email',
                KeyType: 'HASH',
            }
        ],
        AttributeDefinitions: [ // The names and types of all primary and index key attributes only
            {
                AttributeName: 'email',
                AttributeType: 'S', // (S | N | B) for string, number, binary
            }
        ],
        ProvisionedThroughput: { // required provisioned throughput for the table
            ReadCapacityUnits: 1, 
            WriteCapacityUnits: 1
        }
    };

    await dynamoDb.createTable(params, function(err, data) {
        if (err) console.log(err); // an error occurred
        else console.log("User table created"); // successful response
    });

    var empParams = {
        TableName: 'employeesTable',
        KeySchema: [ // The type of of schema.  Must start with a HASH type, with an optional second RANGE.
            { // Required HASH type attribute
                AttributeName: 'email',
                KeyType: 'HASH',
            }
        ],
        AttributeDefinitions: [ // The names and types of all primary and index key attributes only
            {
                AttributeName: 'email',
                AttributeType: 'S', // (S | N | B) for string, number, binary
            }
        ],
        ProvisionedThroughput: { // required provisioned throughput for the table
            ReadCapacityUnits: 1, 
            WriteCapacityUnits: 1
        }
    };
    await dynamoDb.createTable(empParams, function(err, data) {
        if (err) console.log(err); // an error occurred
        else console.log("Employee table created"); // successful response
    });
})

afterAll(async () => {
    var params = {
        TableName: 'usersTable',
    };

    await dynamoDb.deleteTable(params, function(err, data) {
        if (err) console.log(err); // an error occurred
        else {
        return; 
        }
    });
    
    var empParams = {
        TableName: 'employeesTable',
    };

    await dynamoDb.deleteTable(empParams, function(err, data) {
        if (err) console.log(err); // an error occurred
        else {
        return; 
        }
    });
});

const testUser = {
    "email": "testuser@email.com",
    "password": "password",
    "confirmPassword": "confirmPassword",
    "firstName": "firstName",
    "lastName": "lastName",
    "mobile": "mobile",
    "imageURL": "imageURL"
};

const testUserEdit = {
    "email": "testuser@email.com",
    "password": "passwordedit",
    "confirmPassword": "confirmPasswordedit",
    "firstName": "firstNameedit",
    "lastName": "lastNameedit",
    "mobile": "mobileedit",
    "imageURL": "imageURL"
};

const testInvalidPut = {
    "email": "testuser@email.com",
    "password": "passwordedit1",
    "confirmPassword": "confirmPasswordedit1",
    "firstName": "firstNameedit1",
    "lastName": 1,
    "mobile": "mobileedit1",
    "imageURL": "imageURL1"
};

const testUserInvalidPOST = {
    "email": "testPOST@email.com",
    "password": "password",
    "confirmPassword": "confirmPassword",
    "firstName": "firstName",
    "mobile": "mobile",
    "imageURL": "imageURL"
};

describe('POST /api/user', () => {
    test("creates a user", async () => {
        const newUser = await request(server)
            .post('/api/user')
            .send(testUser);
        
        //make sure we added the user correctly
        expect(newUser.body).toHaveProperty("id");
        expect(newUser.body.email).toBe("testuser@email.com");
        expect(newUser.statusCode).toBe(201);
    });
})

describe('GET /getAllUsers', () => {
    test("Gets all users from user table", async () => {
       
        const response = await request(server).get('/api/getAllUsers');
        expect(response.body.length).toBe(1)
        expect(response.body[0].email).toBeTruthy();
        expect(response.body[0].email).toEqual(testUser.email);
        expect(response.statusCode).toBe(200); 
    });
})

describe('PUT /api/editUser/{email}', () => {
    test("Updates a user from user table", async () => {
        const response = await request(server)
            .put(`/api/editUser/${testUserEdit.email}`)
            .send(testUserEdit);
       
        expect(response.body.email).toBeTruthy();
        expect(response.body.mobile).toEqual(testUserEdit.mobile);
        expect(response.statusCode).toBe(200); 
    });
})

//Error tests
describe('POST /api/user', () => {
    test("Error when user payload not valid ", async () => {
        const response = await request(server).post('/api/user').send(testUserInvalidPOST);
        expect(response.body.error).toBeTruthy();
        expect(response.body.error).toEqual('"lastName" is required');;
        expect(response.statusCode).toBe(404); 
    });
})

describe('PUT /api/editUser/{email}', () => {
    test("Error when user payload not valid ", async () => {
        const response = await request(server)
            .put(`/api/editUser/${testInvalidPut.email}`)
            .send(testInvalidPut);
        expect(response.body.error).toBeTruthy();
        expect(response.body.error).toEqual('"lastName" must be a string');;
        expect(response.statusCode).toBe(404); 
    });
})

//Delete
describe('DELETE /api/deleteUser/{email}', () => {
    test("Deletes a user from user table", async () => {
        const response = await request(server)
            .delete(`/api/deleteUser/${testUser.email}`)
        expect(response.body.message).toEqual('User deleted successfully');
        expect(response.statusCode).toBe(200); 
        
        const responseUser = await request(server).get(`/api/getUser/${testUser.email}`);
        expect(responseUser.body.error).toEqual('User not found');

        const responseGetAll = await request(server).get('/api/getAllUsers');
        expect(responseGetAll.statusCode).toBe(404);
        expect(responseGetAll.error.text).toEqual(JSON.stringify('No users in table'));
    });
})
describe('DELETE /api/deleteUser/{email}', () => {
    test("Error is no user to delete", async () => {
        const response = await request(server)
            .delete(`/api/deleteUser/testEmail@delete.com`)
    });
})

describe('GET /getAllUsers', () => {
    test("Returns correct response when no users in the table", async () => {
       
        const response = await request(server).get('/api/getAllUsers');
        expect(response.statusCode).toBe(404);
        expect(response.error.text).toEqual(JSON.stringify('No users in table')); 
    });
})


describe('GET /getUser', () => {
    test("Error when user does not exist", async () => {
        const response = await request(server).get(`/api/getUser/${testUser.email}`);
        expect(response.body.error).toBeTruthy();
        expect(response.body.error).toEqual('User not found');;
        expect(response.statusCode).toBe(404); 
    });
})