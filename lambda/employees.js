const uuid = require('uuid');
const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies
const { validateEmployee } = require("../validators/employeeValidator")

const employeesTable = 'employeesTable';
const usersTable = 'usersTable';
// create a response
function response(statusCode, message) {
    return {
        statusCode: statusCode,
        body: JSON.stringify(message)
    };
}

// create dynamo instance
const dynamoDb = new AWS.DynamoDB.DocumentClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000',
});

//Get All Employees
module.exports.getAllEmployees = (event, context, callback) => {
    return dynamoDb.scan({
        TableName: employeesTable
    }).promise().then(res => {
      if (res.Items.length === 0) {
        callback(null, response(404, 'No employees in table'));
      } else {
        callback(null, response(200, res.Items))
      }
    }).catch(err => response(null, response(err.statusCode, err)))
}

//Create Employee
module.exports.createEmployee = (event, context, callback) => {
    const data = JSON.parse(event.body);

    const email = event.pathParameters.email;
    const params = {
        Key: {
            email: email
        },
        TableName: usersTable
    }

    dynamoDb.get(params).promise().then(res => {  
        if (res.Item) {
            const {createdAt, firstName, lastName, password, imageURL, mobile, confirmPassword, email } = res.Item;
        
            const employee = {
                createdAt: createdAt,
                firstName: firstName,
                lastName: lastName,
                password: password,
                imageURL: imageURL,
                mobile: mobile,
                confirmPassword: confirmPassword,
                email: email,
                services: data.services
            }

            try {
              validateEmployee(employee);
            } catch (error) {
              //need to return the callback with the response
              return callback(null, response(404, {error: error.message}))
            }
            //if user exists copy over to employees table with the services
            return dynamoDb.put({
                TableName: employeesTable,
                Item: employee
              }).promise().then(() => {
                callback(null, response(201, employee))
              }).catch(err => response(null, response(err.statusCode, err)))
        } else {
            callback(null, response(404, { error: 'Employee not found' }))
        }
    }).catch(err => response(null, response(err.statusCode, err)))
}

// Update Employee
module.exports.updateEmployee = (event, context, callback) => {
    const data = JSON.parse(event.body);
    console.log(data)
    const email = event.pathParameters.email;
    const { services } = data;
    const employee = {
      TableName: employeesTable,
      Key: {
        email: email
      },
      ConditionExpression: 'attribute_exists(email)',
      UpdateExpression: 'SET services = :services',
      ExpressionAttributeValues: {
        ':services': services
      },
      ReturnValue: 'ALL_NEW'
    };
    
    return dynamoDb.update(employee).promise().then(() => {
      callback(null, response(200, data))
    }).catch(err => response(null, response(err.statusCode, err)))
  }
  
  //Delete Employee
  module.exports.deleteEmployee = (event, context, callback) => {
    const email = event.pathParameters.email;
    const params = {
      Key: {
        email: email
      },
      TableName: employeesTable
    }

    return dynamoDb.get(params).promise().then(res => {
      if(res.Item) {
        dynamoDb.delete(params).promise().then(() => {
          console.log('after delete')
          const employeeParams = {
            Key: {
                email: email
            },
            TableName: employeesTable
        }
        //if user is an employee, delete them from the employee db
        dynamoDb.get(employeeParams).promise().then(res => {
          if (res.Item) {
            const employee = {
              Key: {
                email: email
              },
              TableName: employeesTable,
            };
            dynamoDb.delete(employee).promise().then().catch(err => response(null, response(err.statusCode, err)))
          } else {
            console.log('Employee not employee')
          }
        })
      }).catch(err => response(null, response(err.statusCode, err)))
    } else {
      callback(null, response(404, {error: 'Employee not found'}))
    }
    callback(null, response(200, {message: 'Employee deleted successfully'}))
    }).catch(err => response(null, response(err.statusCode, err)))
  }


  module.exports.getEmployee = (event, context, callback) => {
    const email = event.pathParameters.email;
    const params = {
      Key: {
        email:email
      },
      TableName: employeesTable
    }
  
    return dynamoDb.get(params).promise().then(res => {
      if(res.Item) callback(null, response(200, res.Item))
      else callback(null, response(404, {error: 'Employee not found'}))
    }).catch(err => response(null, response(err.statusCode, err)))
  }