const uuid = require('uuid');
const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies
const { validateUser } = require("../validators/userValidator");

const usersTable = 'usersTable';
const employeesTable = 'employeesTable';
// create a response
function response(statusCode, message) {
  return {
    statusCode: statusCode,
    body: JSON.stringify(message)
  };
}

// create dynamo instance
const dynamoDb = new AWS.DynamoDB.DocumentClient({
  region: 'localhost',
  endpoint: 'http://localhost:8000',
});

//event --> body
// context env variables
// callback we call later

//Create a user
module.exports.createUser = (event, context, callback) => {
    const data = JSON.parse(event.body);

    const user = {
        id: context.awsRequestId,
        email: data.email,
        createdAt: new Date().toISOString(),
        password: data.password,
        confirmPassword: data.confirmPassword,
        firstName: data.firstName,
        lastName: data.lastName,
        mobile: data.mobile,
        imageURL: data.imageURL 
    };

    const email = data.email;
    const params = {
        Key: {
            email: email
        },
        TableName: usersTable
    }

    dynamoDb.get(params).promise().then(res => { 
      if(!res.Item) {
        try {
          validateUser(user);
        } catch (error) {
          //need to return the callback with the response
          return callback(null, response(404, {error: error.message}))
        }
        // write the user to the database
        return dynamoDb.put({
          TableName: usersTable,
          Item: user
        }).promise().then(() => {
           callback(null, response(201, user))
          //catch the err so the client doesnt get back a server error
        }).catch(err => response(null, response(err.statusCode, err)))
      } else {
        callback(null, response(404, {error: 'Email Already Exists'}))
      }
    })
    
    
}

//Get All Users
module.exports.getAllUsers = (event, context, callback) => {
  return dynamoDb.scan({
    TableName: usersTable
  }).promise().then(res => {
    if (res.Items.length === 0) {
      callback(null, response(404, 'No users in table'));
    } else {
      callback(null, response(200, res.Items))
    }
  }).catch(err => response(null, response(err.statusCode, err)))
}

module.exports.getUser = (event, context, callback) => {
  const email = event.pathParameters.email;
  const params = {
    Key: {
      email:email
    },
    TableName: usersTable
  }

  return dynamoDb.get(params).promise().then(res => {
    if(res.Item) callback(null, response(200, res.Item))
    else callback(null, response(404, {error: 'User not found'}))
  }).catch(err => response(null, response(err.statusCode, err)))
}

// Update User
module.exports.updateUser = (event, context, callback) => {
  const data = JSON.parse(event.body);
  const email = event.pathParameters.email;
  const {password, confirmPassword, firstName, lastName, mobile, imageURL} = data;
  console.log(data);
  const user = {
    TableName: usersTable,
    Key: {
      email: email
    },
    ConditionExpression: 'attribute_exists(email)',
    UpdateExpression: 'SET password = :password, confirmPassword = :confirmPassword, firstName = :firstName, lastName = :lastName, mobile = :mobile, imageURL = :imageURL',
    ExpressionAttributeValues: {
      ':password': password,
      ':confirmPassword': confirmPassword,
      ':firstName': firstName,
      ':lastName': lastName,
      ':mobile': mobile,
      ':imageURL': imageURL
    },
    ReturnValue: 'ALL_NEW'
  };
  
  try {
    validateUser(data);
  } catch (error) {
    //need to return the callback with the response
    return callback(null, response(404, {error: error.message}))
  }
  
  return dynamoDb.update(user).promise().then(() => {
    //check if employee exists
    const employeeParams = {
      Key: {
          email: email
      },
      TableName: employeesTable
  }

  dynamoDb.get(employeeParams).promise().then(res => {
    if (res.Item) {
      const employee = {
        TableName: employeesTable,
        Key: {
          email: email
        },
        ConditionExpression: 'attribute_exists(email)',
        UpdateExpression: 'SET password = :password, confirmPassword = :confirmPassword, firstName = :firstName, lastName = :lastName, mobile = :mobile, imageURL = :imageURL',
        ExpressionAttributeValues: {
          ':password': password,
          ':confirmPassword': confirmPassword,
          ':firstName': firstName,
          ':lastName': lastName,
          ':mobile': mobile,
          ':imageURL': imageURL
        },
        ReturnValue: 'ALL_NEW'
      };
      dynamoDb.update(employee).promise().then().catch(err => response(null, response(err.statusCode, err)))
    } else {
      console.log('User not employee')
    }
    })
    callback(null, response(200, data))
  }).catch(err => response(null, response(err.statusCode, err)))
}

//Delete User
module.exports.deleteUser = (event, context, callback) => {
  const email = event.pathParameters.email;
  const params = {
    Key: {
      email:email
    },
    TableName: usersTable
  }

  return dynamoDb.get(params).promise().then(res => {
    if(res.Item) {
      dynamoDb.delete(params).promise().then(() => {
        console.log('after delete')
        const employeeParams = {
          Key: {
              email: email
          },
          TableName: employeesTable
      }
      //if user is an employee, delete them from the employee db
      dynamoDb.get(employeeParams).promise().then(res => {
        console.log("In delete get", res.Item)
        if (res.Item) {
          const employee = {
            Key: {
              email: email
            },
            TableName: employeesTable,
          };
          dynamoDb.delete(employee).promise().then().catch(err => response(null, response(err.statusCode, err)))
        } else {
          console.log('User not employee')
        }
      })
    }).catch(err => response(null, response(err.statusCode, err)))
  } else {
    callback(null, response(404, {error: 'User not found'}))
  }
  callback(null, response(200, {message: 'User deleted successfully'}))
  }).catch(err => response(null, response(err.statusCode, err)))
}