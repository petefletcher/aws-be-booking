const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies

const eventsTable = 'eventsTable';
// create a response
function response(statusCode, message) {
    console.log(statusCode, JSON.stringify(message))
    return {
        statusCode: statusCode,
        body: JSON.stringify(message)
    };
}

// create dynamo instanse
const dynamoDb = new AWS.DynamoDB.DocumentClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000',
});

//get all events for a user
module.exports.getAllEvents = (event, context, callback) => {
    return dynamoDb.scan({
        TableName: eventsTable
    }).promise().then(res => {
        callback(null, response(200, res.Items))
    }).catch(err => response(null, response(err.statusCode, err)))
}

//create an event
module.exports.createEvent = (event, context, callback) => {
    const data = JSON.parse(event.body);

    //how do we get createdBy
    const newEvent = {
        eventId: context.awsRequestId,
        client: data.client,
        employeeId: data.employeeId,
        timeId: data.timeId,
        startTime: data.startTime,
        endTime: data.endTime,
        finishTimeId: data.finishTimeId,
        aptDate: data.aptDate,
        createdBy: 'Pete'
    };

    // write the user to the database
    return dynamoDb.put({
        TableName: eventsTable,
        Item: newEvent
    }).promise().then(() => {
        callback(null, response(201, newEvent))
        //catch the err so the client doesnt get back a server error
    }).catch(err => response(null, response(err.statusCode, err)))
}

// Update Event
module.exports.updateEvent = (event, context, callback) => {
    const data = JSON.parse(event.body);
    const eventId = event.pathParameters.eventId;
    const { client, employeeId, timeId, startTime, endTime, finishTimeId, aptDate, createdBy } = data;

    const updatedEvent = {
        TableName: eventsTable,
        Key: {
            eventId: eventId
        },
        ConditionExpression: 'attribute_exists(eventId)',
        UpdateExpression: 'SET client = :client, employeeId = :employeeId, timeId = :timeId, startTime = :startTime, endTime = :endTime, finishTimeId = :finishTimeId, aptDate = :aptDate, createdBy = :createdBy',
        ExpressionAttributeValues: {
            ':client': client,
            ':employeeId': employeeId,
            ':timeId': timeId,
            ':startTime': startTime,
            ':endTime': endTime,
            ':finishTimeId': finishTimeId,
            ':aptDate': aptDate,
            ':createdBy': createdBy
        },
        ReturnValue: 'ALL_NEW'
    };

    return dynamoDb.update(updatedEvent).promise().then(() => {
        callback(null, response(200, data))
    }).catch(err => response(null, response(err.statusCode, err)))
}

//Delete Event
module.exports.deleteEvent = (event, context, callback) => {
    const eventId = event.pathParameters.eventId;
    const params = {
        Key: {
            eventId: eventId
        },
        TableName: eventsTable
    }

    return dynamoDb.delete(params).promise().then(() => {
        callback(null, response(200, { message: 'Event deleted successfully' }))
    }).catch(err => response(null, response(err.statusCode, err)))
}
