const Joi = require('@hapi/joi');

const schema = Joi.object({
	email: Joi.string().email().required(),
	password: Joi.string().min(3).max(30).required(),
	confirmPassword: Joi.string().min(3).max(30).required(),
	firstName: Joi.string().min(3).max(30).required(),
	lastName: Joi.string().min(3).max(30).required(),
    mobile: Joi.string().min(3).max(11).required(),
    imageURL: Joi.string(),
    createdAt: Joi.string(),
    services: Joi.array().items(Joi.string()).required(),
})

exports.validateEmployee = (employee) => {
      if(!employee) {
          throw new Error('No employee found')
      } else {
         const result = schema.validate(employee);
         if(result.error) {
             throw new Error(result.error.details[0].message)
         }
      }
  }