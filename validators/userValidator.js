const Joi = require('@hapi/joi');

const schema = Joi.object({
    id: Joi.string(),
	email: Joi.string().email().required(),
	password: Joi.string().min(3).max(30).required(),
	confirmPassword: Joi.string().min(3).max(30).required(),
	firstName: Joi.string().min(3).max(30).required(),
	lastName: Joi.string().min(3).max(30).required(),
    mobile: Joi.string().min(3).max(11).required(),
    imageURL: Joi.string(),
    createdAt: Joi.string()
})

exports.validateUser = (user) => {
      if(!user) {
          throw new Error('No user found')
      } else {
         const result = schema.validate(user);
         if(result.error) {
             throw new Error(result.error.details[0].message)
         }
      }
  }